package com.rnibot;

import android.content.Intent;
import android.os.Bundle;
import android.speech.RecognitionListener;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import android.util.Log;

import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.GuardedRunnable;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.UiThreadUtil;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.modules.core.DeviceEventManagerModule;

import java.util.ArrayList;
import java.util.Locale;

/**
 * Created/Authored by Siddarth/ Bala Krishna.
 */

public class SpeechRecognizerModule extends ReactContextBaseJavaModule {
	private SpeechRecognizer speechRecognizer;
	private Intent speechIntent;
	private ReactContext reactContext;
	
	private static final String SPEECH_STARTED = "speechStarted";
	private static final String SPEECH_FOUND = "speechFound";
	private static final String SPEECH_ENDED = "speechEnded";
	private static final String SPEECH_ERROR = "speechError";
	private static final String EVENT_SPEECH_STATUS = "speechStatus";
	
	
	private static final String TAG = SpeechRecognizerModule.class.getSimpleName();
	
	public SpeechRecognizerModule(ReactApplicationContext reactContext) {
		super(reactContext);
		this.reactContext = reactContext;
	}
	
	
	private void startListening() {
		if (getCurrentActivity() != null) {
			if (speechIntent == null) {
				speechIntent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
				speechIntent.putExtra(RecognizerIntent.EXTRA_CALLING_PACKAGE, getClass().getPackage().getName());
				speechIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_PREFERENCE, Locale.getDefault());
				speechIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
				speechIntent.putExtra(RecognizerIntent.EXTRA_MAX_RESULTS, 3);
			}
			if (speechRecognizer == null) {
				speechRecognizer = SpeechRecognizer.createSpeechRecognizer(getCurrentActivity());
				speechRecognizer.setRecognitionListener(new SpeechListener());
			}
			
			speechRecognizer.startListening(speechIntent);
		}
	}
	
	private void cancel() {
		if (speechRecognizer != null)
			speechRecognizer.cancel();
	}
	
	private void destroy() {
		if (speechRecognizer != null) {
			speechRecognizer.destroy();
			speechRecognizer = null;
		}
	}
	
	private void stopListening() {
		if (speechRecognizer != null)
			speechRecognizer.stopListening();
	}
	
	@Override
	public String getName() {
		return TAG;
	}
	
	@ReactMethod
	public void startDetection() {
		if (getCurrentActivity() != null) {
			UiThreadUtil.runOnUiThread(
					new GuardedRunnable(getReactApplicationContext()) {
						@Override
						public void runGuarded() {
							startListening();
						}
					});
		}
	}
	
	@ReactMethod
	public void stopDetection() {
		if (getCurrentActivity() != null) {
			UiThreadUtil.runOnUiThread(
					new GuardedRunnable(getReactApplicationContext()) {
						@Override
						public void runGuarded() {
							stopListening();
						}
					});
		}
	}
	
	@ReactMethod
	public void cancelDetection() {
		if (getCurrentActivity() != null) {
			UiThreadUtil.runOnUiThread(
					new GuardedRunnable(getReactApplicationContext()) {
						@Override
						public void runGuarded() {
							cancel();
						}
					});
		}
	}
	
	
	@ReactMethod
	public void onUnMount() {
		if (getCurrentActivity() != null) {
			UiThreadUtil.runOnUiThread(
					new GuardedRunnable(getReactApplicationContext()) {
						@Override
						public void runGuarded() {
							destroy();
						}
					});
		}
	}
	
	private void sendEvent(String eventName, WritableMap params) {
		reactContext
				.getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class)
				.emit(eventName, params);
	}
	
	class SpeechListener implements RecognitionListener {
		
		private static final String TAG = "SpeechListener";
		
		@Override
		public void onReadyForSpeech(Bundle bundle) {
			WritableMap params = Arguments.createMap();
			params.putBoolean(SPEECH_STARTED, true);
			sendEvent(EVENT_SPEECH_STATUS, params);
		}
		
		@Override
		public void onBeginningOfSpeech() {
		
		}
		
		@Override
		public void onRmsChanged(float v) {
		
		}
		
		@Override
		public void onBufferReceived(byte[] bytes) {
		
		}
		
		@Override
		public void onEndOfSpeech() {
			WritableMap params = Arguments.createMap();
			params.putBoolean(SPEECH_ENDED, true);
			sendEvent(EVENT_SPEECH_STATUS, params);
		}
		
		@Override
		public void onError(int errorCode) {
			
			WritableMap params = Arguments.createMap();
			params.putBoolean(SPEECH_ERROR, true);
			sendEvent(EVENT_SPEECH_STATUS, params);
		}
		
		@Override
		public void onResults(Bundle results) {
			ArrayList<String> matches = results.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);
			String text = "";
			if (matches != null && !matches.isEmpty()) {
				text = matches.get(0);
			}
			
			WritableMap params = Arguments.createMap();
			params.putBoolean(SPEECH_FOUND, true);
			sendEvent(EVENT_SPEECH_STATUS, params);
		}
		
		@Override
		public void onPartialResults(Bundle partialResults) {
		
		}
		
		@Override
		public void onEvent(int eventType, Bundle params) {
		
		}
		
	}
	
	
}
