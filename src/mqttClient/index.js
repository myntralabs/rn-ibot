
import { Client } from 'react-native-paho-mqtt'

const myStorage = {
	setItem: (key, item) => {
		myStorage[key] = item
	},
	getItem: key => myStorage[key],
	removeItem: key => {
		delete myStorage[key]
	},
}

const Mqtt = {
	createClient (options) {
		this.client = new Client({
			uri: options.uri,
			clientId: options.clientId,
			// webSocket: socket(options.uri),
			storage: myStorage,
		})
		this.clientRef = Math.random()
		return this.clientRef
	},
	connect () {
		if (this.client) {
			this.register()
			this.client.connect().then(res => {
				this.callback && this.callback({ event: 'connect', clientRef: this.clientRef, message: res })
			}).catch(err => {
				this.callback && this.callback({ event: 'error', clientRef: this.clientRef, message: err })
			})
		}
	},
	subscribe (clientRef, topic, qos) {
		if (this.client)
			return this.client.subscribe(topic, { qos, timeout: 10000 })
	},
	disconnect () {
		if (this.client)
			return this.client.disconnect()
	},
	register () {
		if (this.client) {
			this.client.on('connectionLost', message => {
				this.callback && this.callback({ event: 'closed', clientRef: this.clientRef, message })
			})

			this.client.on('messageReceived', message => {
				try {
					this.callback && this.callback({ event: 'message', clientRef: this.clientRef, message: { data: message.payloadString } })
				} catch (err) {
					this.callback && this.callback({ event: 'error', clientRef: this.clientRef, message: 'Message parse error' })
				}
			})
		}
	},
	onDispatch (func) {
		this.callback = func
		return this
	},
}


const MqttHelper = function (options) {
	this.options = options
	this.eventHandler = {}
	this.dispatchEvent = function (data) {
		if (data && data.clientRef === this.clientRef && data.event && this.eventHandler[data.event])
			this.eventHandler[data.event](data.message)
	}

	this.createClient = async function (options) {
		if (options.uri) {
			const pattern = /^((mqtt[s]?|ws[s]?)?:(\/\/)([a-zA-Z0-9.]*):?(\d+)(\/([a-zA-Z0-9.]*))*)$/
			const matches = options.uri.match(pattern)
			const protocol = matches[2]
			const host = matches[4]
			const port = matches[5]
			options.port = parseInt(port)
			options.host = host
			options.protocol = 'tcp'

			if (protocol === 'wss' || protocol === 'mqtts')
				options.tls = true
			if (protocol === 'ws' || protocol === 'wss')
				options.protocol = protocol
		}
		this.clientRef = await Mqtt.createClient(options)

		/* Listen mqtt event */
		if (Object.keys(this.eventHandler).length === 0)
			Mqtt.onDispatch(data => this.dispatchEvents(data))
		return this
	}

	this.removeClient = function () {
		Mqtt.onDispatch(null)
		this.eventHandler = {}
	}

}

MqttHelper.prototype.on = function (event, callback) {
	this.eventHandler[event] = callback
}

MqttHelper.prototype.connect = function () {
	Mqtt.connect(this.clientRef)
}

MqttHelper.prototype.disconnect = function () {
	Mqtt.disconnect(this.clientRef)
}

MqttHelper.prototype.reconnect = function () {
	Mqtt.reconnect()
}

MqttHelper.prototype.subscribe = function (topic, qos) {
	Mqtt.subscribe(this.clientRef, topic, qos)
}

MqttHelper.prototype.publish = function (topic, payload, qos, retain) {
	Mqtt.publish(this.clientRef, topic, payload, qos, retain)
}

export default MqttHelper