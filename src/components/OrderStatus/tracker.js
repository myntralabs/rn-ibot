import React from 'react'
import PropTypes from 'prop-types'
import {
	View,
	Text,
	Image,
	TouchableOpacity,
} from 'react-native'
import { trackerStyle } from './styles'
import Dash from 'react-native-dash'
import { topaz } from '../../../../resources/colors'

const Tracker = props => {
	const {
		initialValue,
		finalValue,
		currentValue,
		isComplete,
		offset,
		currentStateLabel,
		onAction,
	} = props

	let fillWidth = isComplete ? 100
		: Math.min(65, Math.ceil(100 / (finalValue - initialValue) * (currentValue - initialValue)))
	if (fillWidth !== 100 && offset
		&& currentValue !== initialValue) {
		fillWidth += offset
		const minFillWidth = offset
		const maxFillWidth = 100 - offset
		fillWidth = Math.max(fillWidth, minFillWidth)
		fillWidth = Math.min(fillWidth, maxFillWidth)
	}

	const filledLineStyle = { width: `${ fillWidth }%` }
	const dottedLineStyle = {
		width: `${ Math.min(100 - fillWidth, 10) }%`,
		left: `${ fillWidth }%`,
	}

	return (
		<View style={ trackerStyle.trackerContainer }>
			{ !isComplete && <Text style={ [ trackerStyle.progressText, { left: `${ Math.min(45, fillWidth) }%` } ] }>{ currentStateLabel }</Text> }
			<View style={ trackerStyle.barContainer }>
				<View style={ [ isComplete ? trackerStyle.circleTop1 : trackerStyle.circleTop2, trackerStyle.circleMark, trackerStyle.circleFirst, trackerStyle.circleComplete ] } />
				<View style={ isComplete ? trackerStyle.linesContainerComplete : trackerStyle.lineContainer }>
					<View style={ [ trackerStyle.lineInComplete ] } />
					<View style={ [ trackerStyle.lineComplete, filledLineStyle ] } />
					{ !isComplete && <Dash dashColor={ topaz } style={ [ trackerStyle.lineDotted, dottedLineStyle ] } /> }
				</View>
				<View style={ [ isComplete ? trackerStyle.circleTop1 : trackerStyle.circleTop2, trackerStyle.circleMark, trackerStyle.circleSecond, isComplete ? trackerStyle.circleComplete : trackerStyle.circleIncomplete ] } />
			</View>
			<View>
				<TouchableOpacity onPress={ onAction }>
					<Image style={ trackerStyle.icon } source={ { uri: 'right_arrow' } } />
				</TouchableOpacity>
			</View>
		</View >
	)
}

Tracker.propTypes = {
	initialValue: PropTypes.any,
	finalValue: PropTypes.any,
	currentValue: PropTypes.any,
	isComplete: PropTypes.bool,
	offset: PropTypes.number,
	onAction: PropTypes.func,
	currentStateLabel: PropTypes.string,
}

export default Tracker