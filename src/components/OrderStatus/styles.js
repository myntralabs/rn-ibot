import {
	StyleSheet,
} from 'react-native'

import fonts from '../../resources/fonts'
import {
	blueberry50,
	darkTwo,
	topaz,
	blueberry10,
	blueberry20,
} from '../../resources/colors'

const styles = StyleSheet.create({
	statusContainer: {
		flexDirection: 'row',
		justifyContent: 'space-between',
		alignItems: 'center',
	},
	statusTextContainer: {
		padding: 8,
		alignItems: 'center',
	},
	statusText: {
		fontFamily: fonts.bold,
		color: blueberry50,
		lineHeight: 16,
		fontSize: 14,
		textAlign: 'center',
	},
	statusDate: {
		fontFamily: fonts.bold,
		color: blueberry50,
		lineHeight: 16,
		fontSize: 12,
		textAlign: 'center',
	},
})

const trackerStyle = StyleSheet.create({
	progressText: {
		fontFamily: fonts.bold,
		color: darkTwo,
		lineHeight: 18,
		fontSize: 13,
	},
	trackerContainer: {
		width: 180,
		alignSelf: 'center',
		height: 46,
	},
	barContainer: {
		width: 180,
		flex: 1,
		flexDirection: 'row',
	},
	circleTop1: { top: 16 },
	circleTop2: { top: 2 },
	circleMark: {
		width: 12,
		height: 12,
		borderRadius: 6,
		borderWidth: 2.5,
	},
	circleComplete: { borderColor: topaz },
	circleIncomplete: { borderColor: blueberry10 },
	circleFirst: {
		position: 'absolute',
		left: 6,
	},
	circleSecond: {
		position: 'absolute',
		right: 6,
	},
	lineInComplete: {
		height: '100%',
		width: '100%',
		position: 'absolute',
		backgroundColor: blueberry10,
	},
	lineComplete: {
		height: '100%',
		position: 'absolute',
		backgroundColor: topaz,
	},
	lineDotted: {
		height: '100%',
		position: 'absolute',
	},
	linesContainerComplete: {
		position: 'absolute',
		width: 144,
		height: 2.5,
		left: 18,
		top: 21,
	},
	lineContainer: {
		position: 'absolute',
		width: 144,
		height: 2.5,
		left: 18,
		top: 7,
	},
	icon: {
		transform: [ { rotate: '90deg' } ],
		tintColor: blueberry20,
		alignSelf: 'center',
		width: 22,
		height: 15,
	},
})

export { trackerStyle }
export default styles