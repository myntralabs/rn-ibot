const TrackingDisplayStates = {
	PLACED: 'Placed',
	PROCESSING: 'Processing',
	DISPATCH_FROM_SOURCE: 'Processing',
	RECEIVED_FOR_PACK: 'Packing',
	PACKED: 'Packed',
	SHIPPED: 'Shipped',
	OUT_FOR_DELIVERY: 'Out for delivery',
	DELIVERED: 'Delivered',
	CANCELLED: 'Cancelled',
}

const TrackingStateMappings = {
	ORDER_CREATION: TrackingDisplayStates.PLACED,
	PACK_IN_SOURCE_WAREHOUSE: TrackingDisplayStates.PROCESSING,
	DISPATCH_FROM_SOURCE_WAREHOUSE: TrackingDisplayStates.DISPATCH_FROM_SOURCE,
	RECEIVED_IN_DISPATCH_WAREHOUSE: TrackingDisplayStates.RECEIVED_FOR_PACK,
	PACK_IN_DISPATCH_WAREHOUSE: TrackingDisplayStates.PACKED,
	SHIP_TO_CUSTOMER: TrackingDisplayStates.SHIPPED,
	OUT_FOR_DELIVERY: TrackingDisplayStates.OUT_FOR_DELIVERY,
	CANCELLED: TrackingDisplayStates.CANCELLED,
	DELIVERED_TO_CUSTOMER: TrackingDisplayStates.DELIVERED,
}
const monthNames = [ 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec' ]


const getTrackerData = (otsResponse = []) => {
	let lastUpdatedState = 'Placed'
	let lastUpdationDate = ''
	const statesInfo = {}
	try {
		otsResponse.forEach(trackingState => {
			let { actualEventTime, expectedEventTime } = trackingState
			const { eventType } = trackingState
			const trackingDisplayState = TrackingStateMappings[eventType]
			if (trackingDisplayState) {
				actualEventTime = actualEventTime ? parseInt(actualEventTime, 10) : ''
				expectedEventTime = expectedEventTime ? parseInt(expectedEventTime, 10) : ''
				const actualDate = (actualEventTime && new Date(actualEventTime).getTime()) || ''
				const expectedDate = (expectedEventTime && new Date(expectedEventTime).getTime()) || ''
				if (!lastUpdationDate || lastUpdationDate < actualDate) {
					lastUpdationDate = actualDate
					lastUpdatedState = trackingDisplayState
				}
				if (statesInfo.hasOwnProperty(trackingDisplayState)) {
					const currentState = statesInfo[trackingDisplayState]
					if (actualDate > currentState.actualDate)
						currentState.actualDate = actualDate
					else if (expectedDate > currentState.expectedDate)
						currentState.expectedDate = expectedDate
				} else
					statesInfo[trackingDisplayState] = {
						actualDate,
						expectedDate,
					}
			}
		})
	} catch (e) {
		__DEV__ && console.log('Orderstatus', e)
	}
	return {
		lastUpdatedState,
		statesInfo,
	}
}

const showTracker = (trackingInfo = {}) => {
	const { statesInfo = {} } = trackingInfo
	// Atleast placed and delivery expected states shoud be there
	if (Object.keys(statesInfo).length >= 2) {
		// Show if it was delivered today
		const deliveryDate = trackingInfo.statesInfo
								&& trackingInfo.statesInfo.Delivered
								&& trackingInfo.statesInfo.Delivered.actualDate
		return deliveryDate && new Date().getTime() < new Date(parseInt(deliveryDate, 10)).setHours(24, 0, 0, 0)
	}
	return false
}

const getDateString = date => {
	if (!date)
		return
	const currentDate = new Date()
	let dateString = null
	if (currentDate.getMonth() === date.getMonth() && currentDate.getDate() - date.getDate() < 1)
		dateString = 'Today'
	else
		dateString = date.getDate() + ' ' + monthNames[date.getMonth()]
	return dateString
}

const isDeliveryDelayed = (deliveryExpectedDate, deliveryActualDate) =>
	!deliveryActualDate && new Date().getTime() > new Date(parseInt(deliveryExpectedDate, 10)).setHours(24, 0, 0, 0)

export { getTrackerData, showTracker, getDateString, isDeliveryDelayed }