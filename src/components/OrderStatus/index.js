import React from 'react'
import PropTypes from 'prop-types'
import {
	View,
	Text,
} from 'react-native'
import Tracker from './tracker'
import { getTrackerData, showTracker, getDateString, isDeliveryDelayed } from './helper'
import styles from './styles'

const OrderStatus = ({ tracking, onAction }) => {
	const trackingInfo = getTrackerData(tracking)
	const statesInfo = trackingInfo && trackingInfo.statesInfo
	if (showTracker(trackingInfo)) {
		const placedActualDate = statesInfo && statesInfo.Placed && statesInfo.Placed.actualDate
		const placedDateObject = placedActualDate && new Date(placedActualDate)
		const deliveryExpectedDate = statesInfo && statesInfo.Delivered && statesInfo.Delivered.expectedDate
		const deliveryActualDate = statesInfo && statesInfo.Delivered && statesInfo.Delivered.actualDate
		const deliveryDateObject = new Date(deliveryActualDate || deliveryExpectedDate)
		const lastUpdatedState = trackingInfo && trackingInfo.lastUpdatedState
		const lastUpdatedStateDate = statesInfo && statesInfo[lastUpdatedState] && statesInfo[lastUpdatedState].actualDate
		const lastUpdatedDateObject = lastUpdatedStateDate && new Date(lastUpdatedStateDate)
		const deliveryDelayed = isDeliveryDelayed(deliveryExpectedDate, deliveryActualDate)

		const getTracker = () => (
			<Tracker
				initialValue={ placedActualDate }
				finalValue={ deliveryActualDate || deliveryExpectedDate }
				currentValue={ lastUpdatedStateDate }
				isComplete={ !!deliveryActualDate || false }
				offset={ 10 }
				onAction={ onAction }
				currentStateLabel={ `${ trackingInfo.lastUpdatedState } (${ getDateString(lastUpdatedDateObject) })` }
			/>
		)

		if (placedActualDate && (deliveryExpectedDate || deliveryActualDate) && lastUpdatedState && lastUpdatedStateDate)
			return (
				<View style={ styles.statusContainer }>
					<View style={ styles.statusTextContainer } >
						<Text style={ styles.statusText }>{'Ordered'}</Text>
						<Text style={ styles.statusDate }>{ getDateString(placedDateObject) }</Text>
					</View>
					{ getTracker() }
					<View style={ styles.statusTextContainer } >
						<Text style={ styles.statusText }>{ deliveryActualDate ? 'Delivered' : 'Delivery' }</Text>
						<Text style={ styles.statusDate }>{ getDateString(deliveryDateObject) + (deliveryDelayed ? '*' : '') }</Text>
					</View>
				</View>
			)
	}

	return (<View />)
}


OrderStatus.propTypes = {
	tracking: PropTypes.object.isRequired,
	onAction: PropTypes.func,
}

export default OrderStatus