import React from 'react'
import PropTypes from 'prop-types'
import {
	View,
	Text,
	TouchableOpacity,
	Image,
} from 'react-native'
import styles from './styles'
import { ACTION, ATTACHMENT_TYPE } from '../../constants'
import TextCard from '../TextCard'
import { getTriggers } from '../../utils/helpers'
import { bind } from '../../../../utils/decorators'


class OptionsCard extends React.PureComponent {
	constructor (props) {
		super(props)
		this.isSender = props.data && !props.data.uidx
	}

	@bind
	onPressItem (item, index) {
		if (item.attachmentType === ATTACHMENT_TYPE.CUSTOMER_QUERY_SELECT) {
			const analyticsData = {
				entity_type: 'query',
				entity_name: this.props.data.context && getTriggers(this.props.data.context.triggers) || this.props.data.trigger,
				entity_optional_attributes: { level: this.props.data.context && this.props.data.context.level || 1 },
			}

			this.props.onAction(ACTION.ATTACHMENT_SELECT_PERFORMED, { selectedAttachment: index, ...this.props.data }, analyticsData)
		}
	}

	@bind
	renderItem (item, index) {
		const { attachments: { items = [] } } = this.props.data
		return (
			<View
				style={ index === items.length - 1 ? styles.itemContainerLast : styles.itemContainer }
				key={ index }
			>
				<TouchableOpacity onPress={ () => this.onPressItem(item, index) } >
					<View style={ styles.itemContent }>
						<Text style={ styles.itemText }>{ item.title }</Text>
						<View style={ styles.space } />
						<Image
							style={ styles.arrowIcon }
							source={ { uri: 'right_arrow' } }
						/>
					</View>
				</TouchableOpacity>
			</View>
		)
	}


	render () {

		if (!this.props.data)
			return (<View />)
		const { attachments: { items = [] } } = this.props.data
		if (this.isSender)
			return (
				<View>
					<TextCard
						data={ this.props.data }
						onAction={ this.props.onAction }
					/>
					<View style={ styles.container }>
						{
							items.map(this.renderItem)
						}
					</View>
				</View>
			)

		if (items && items[0] && items[0].title)
			return (
				<View style={ styles.containerReceiver }>
					<View style={ styles.receiverTextContainer }>
						<Text style={ styles.receiverText }>
							{ items[0].title }
						</Text>
					</View>
				</View>
			)

		return (<View />)
	}
}

OptionsCard.propTypes = {
	data: PropTypes.object.isRequired,
	onAction: PropTypes.func,
}

export default OptionsCard