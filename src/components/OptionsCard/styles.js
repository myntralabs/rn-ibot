import {
	StyleSheet,
	Dimensions,
} from 'react-native'

import fonts from '../../resources/fonts'
import {
	cornflowerBlue,
	blueberry10,
	palePurple,
	darkTwo,
} from '../../resources/colors'

const screenWidth = Dimensions.get('window').width
const styles = StyleSheet.create({
	container: {
		borderRadius: 4,
		borderWidth: 1,
		borderColor: cornflowerBlue,
		flex: 1,
		margin: 8,
		width: screenWidth * 0.8,
	},
	itemContainer: {
		borderBottomWidth: 1,
		borderColor: cornflowerBlue,
		flex: 1,
	},
	itemContainerLast: {
		borderBottomWidth: 0,
		flex: 1,
	},
	containerClient: {
		borderRadius: 4,
		borderWidth: 1,
		borderColor: cornflowerBlue,
		flex: 1,
		margin: 8,
		justifyContent: 'flex-end',
		alignSelf: 'flex-end',
		alignItems: 'flex-end',
		maxWidth: screenWidth * 0.8,
	},
	itemContent: { flexDirection: 'row' },
	itemText: {
		color: cornflowerBlue,
		fontSize: 15,
		fontFamily: fonts.bold,
		letterSpacing: 0,
		paddingVertical: 8,
		width: screenWidth * 0.8 - 28,
		paddingHorizontal: 16,
		flex: 1,
		flexWrap: 'wrap',
	},
	space: { width: 40 },
	arrowIcon: {
		width: 12,
		height: 16,
		color: blueberry10,
		position: 'absolute',
		right: 8,
		alignSelf: 'center',
	},
	containerReceiver: {
		justifyContent: 'flex-end',
		flexDirection: 'row',
		marginVertical: 6,
		marginHorizontal: 8,
	},
	receiverTextContainer: {
		maxWidth: screenWidth * 0.75,
		flexWrap: 'wrap',
		backgroundColor: palePurple,
		borderRadius: 18,
	},
	receiverText: {
		fontSize: 15,
		fontFamily: fonts.regular,
		lineHeight: 18,
		textAlign: 'left',
		letterSpacing: 0,
		color: darkTwo,
		paddingHorizontal: 16,
		paddingVertical: 10,
	},
})

export default styles