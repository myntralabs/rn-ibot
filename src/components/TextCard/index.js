import React from 'react'

import PropTypes from 'prop-types'
import {
	View,
	Text,
} from 'react-native'
import styles from './styles'

const TextCard = props => {
	const {
		uidx,
		itext,
	} = props.data
	const sender = !uidx
	if (!itext)
		return (<View />)
	return (
		<View style={ sender ? styles.containerSender : styles.containerReceiver }>
			<View style={ sender ? styles.senderTextContainer : styles.receiverTextContainer }>
				<Text style={ sender ? styles.senderText : styles.receiverText }>
					{ itext }
				</Text>
			</View>
		</View>
	)
}


TextCard.propTypes = { data: PropTypes.object.isRequired }

export default TextCard