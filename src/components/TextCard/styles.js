import { Dimensions } from 'react-native'
import { darkTwo, paleGrey, palePurple } from '../../resources/colors'
import fonts from '../../resources/fonts'

const screenWidth = Dimensions.get('window').width

const styles = StyleSheet.create({
	containerSender: {
		justifyContent: 'flex-start',
		flexDirection: 'row',
		marginVertical: 6,
		marginHorizontal: 8,
	},
	containerReceiver: {
		justifyContent: 'flex-end',
		flexDirection: 'row',
		marginVertical: 6,
		marginHorizontal: 8,
	},
	senderTextContainer: {
		maxWidth: screenWidth * 0.75,
		flexWrap: 'wrap',
		backgroundColor: paleGrey,
		borderRadius: 18,
	},
	receiverTextContainer: {
		maxWidth: screenWidth * 0.75,
		flexWrap: 'wrap',
		backgroundColor: palePurple,
		borderRadius: 18,
	},
	senderText: {
		fontSize: 15,
		fontFamily: fonts.regular,
		lineHeight: 18,
		textAlign: 'left',
		paddingHorizontal: 16,
		paddingVertical: 10,
		letterSpacing: 0,
		color: darkTwo,
	},
	receiverText: {
		fontSize: 15,
		fontFamily: fonts.regular,
		lineHeight: 18,
		textAlign: 'left',
		letterSpacing: 0,
		color: darkTwo,
		paddingHorizontal: 16,
		paddingVertical: 10,
	},
})

export default styles