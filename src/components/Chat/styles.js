import {
	StyleSheet,
	Dimensions,
	Platform,
} from 'react-native'

import { white } from '../../resources/colors'

const screenWidth = Dimensions.get('window').width

const styles = StyleSheet.create({
	flex: {
		flex: 1,
		backgroundColor: white,
	},
	margin: {
		...Platform.select({
			ios: {
				marginTop: 0,
			},
			android: {
				marginTop: 56,
			},
		}),
	},
	listContainerStyle: { paddingTop: 24 },
	listStyle1: {
		flex: 1,
		marginBottom: 64,
	},
	listStyle2: {
		marginBottom: 12,
		marginLeft: 4,
		flex: 1,
	},
	suggestionStyle: {
		paddingBottom: 8,
		flexDirection: 'row',
		alignItems: 'flex-end',
		flex: 1,
		justifyContent: 'flex-end',
		backgroundColor: white,
		minWidth: screenWidth,
	},
	suggestionsContainerl0: {
		position: 'absolute',
		bottom: 0,
		paddingBottom: 8,
		flex: 1,
		width: screenWidth,
		paddingRight: 8,
	},
	suggestionsContainerl1: {
		position: 'absolute',
		bottom: 48,
		flex: 1,
		width: screenWidth,
		paddingRight: 8,
	},
})

export default styles