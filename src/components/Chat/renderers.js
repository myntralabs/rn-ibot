import RichCard from '../RichCard'
import FeedbackCard from '../FeedbackCard'
import OptionsCard from '../OptionsCard'
import Timer from '../HelpShift/Timer'
import ChatHeader from '../HelpShift/ChatHeader'
import ChatWaiting from '../HelpShift/ChatWaiting'
import StandaLoneCard from '../StandAloneCard'

export const triggerRenderer = {
	'chat-with-us': Timer,
	'chat-with-us-ack': ChatWaiting,
	'chat-with-us-assignee': ChatHeader,
	'chat-with-us-response': ChatHeader,
	great: FeedbackCard,
	notgreat: FeedbackCard,
}

export const attachmentRenderer = {
	CAROUSEL_SELECT: RichCard,
	LIST_SELECT: OptionsCard,
	CAROUSEL: RichCard,
	LIST: OptionsCard,
	STAND_ALONE: StandaLoneCard,
}