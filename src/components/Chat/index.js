import React from 'react'
import PropTypes from 'prop-types'
import {
	FlatList,
	View,
} from 'react-native'

import Suggestions from '../Suggestions'
import ActionFooter from '../ActionFooter'
import TextCard from '../TextCard'
import { ACTION, BOT_ILEVEL } from '../../constants'
import bind from '../../utils/decorators'
import styles from './styles'
import { triggerRenderer, attachmentRenderer } from './renderers'

const itemToCardMapping = item => {
	if (triggerRenderer[item.trigger])
		return triggerRenderer[item.trigger]
	else if (item.attachments && attachmentRenderer[item.attachments.attachmentsType])
		return attachmentRenderer[item.attachments.attachmentsType]
	return TextCard
}

class Chat extends React.Component {

	constructor (props) {
		super(props)
		this.page = 1
		this.scrollViewRef = component => this.scrollView = component
		this.footerRef = component => this.footer = component
	}

	@bind
	handleLoadMore () {
		this.props.onAction && this.props.onAction(ACTION.PAGINATE, { page: this.page })
		this.page += 1
	}

	@bind
	openKeyboard () {
		this.footer && this.footer.openKeyboard()
	}

	@bind
	isMicOpened () {
		return this.footer && this.footer.isMicOpened()
	}

	@bind
	renderItem ({ item }) {
		const Card = itemToCardMapping(item)
		return (
			<Card
				data={ item }
				onAction={ this.props.onAction }
			/>
		)
	}

	@bind
	onContentSizeChange () {
		const len = (this.props.imessages || []).length
		if (len > 0)
			this.scrollView.scrollToIndex({ index: len - 1, animated: true })
	}

	@bind
	onScrollToIndexFailed () {
		this.scrollView && this.scrollView.scrollToEnd()
	}

	@bind
	isSuggestionPresent () {
		return this.props.currentSuggestions && this.props.currentSuggestions.suggestions && this.props.currentSuggestions.suggestions.length > 0
	}

	render () {
		return (
			<View style={ [ styles.flex, styles.margin ] }>
				<FlatList
					ref={ this.scrollViewRef }
					data={ this.props.imessages || [] }
					renderItem={ this.renderItem }
					onEndReached={ this.handleLoadMore }
					keyExtractor={ (item, index) => index }
					onEndReachedThreshold={ 80 }
					alwaysBounceVertical
					onScrollToIndexFailed={ this.onScrollToIndexFailed }
					contentContainerStyle={ styles.listContainerStyle }
					onContentSizeChange={ this.onContentSizeChange }
					style={ this.isSuggestionPresent() ? styles.listStyle1 : styles.listStyle2 }
					keyboardShouldPersistTaps={ 'always' }
				/>
				<Suggestions
					currentSuggestion={ this.props.currentSuggestions || {} }
					onAction={ this.props.onAction }
					suggestionsContainer={ this.props.botILevel === BOT_ILEVEL.SUGGESTION ? styles.suggestionsContainerl0 : styles.suggestionsContainerl1 }
					suggestionStyle={ styles.suggestionStyle }
				/>
				<ActionFooter
					{ ...this.props }
					ref={ this.footerRef }
				/>
			</View>
		)
	}
}

/* eslint-disable react/no-unused-prop-types */
Chat.propTypes = {
	imessages: PropTypes.object,
	currentSuggestions: PropTypes.object,
	onAction: PropTypes.func,
	style: PropTypes.any,
	isSpeechListening: PropTypes.bool,
	botILevel: PropTypes.number,
}

export default Chat