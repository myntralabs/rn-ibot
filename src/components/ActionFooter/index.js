import React from 'react'

import PropTypes from 'prop-types'
import {
	Keyboard,
	TextInput,
	TouchableOpacity,
	View,
	Image,
} from 'react-native'

import styles from './styles'
import bind from '../../utils/decorators'
import { ACTION, BOT_ILEVEL } from '../../constants'
import { white, watermelon100 } from '../../resources/colors'

export default class ActionFooter extends React.PureComponent {

	static propTypes = {
		isSpeechListening: PropTypes.func,
		onAction: PropTypes.func,
		botILevel: PropTypes.number,
	}

	constructor (props) {
		super(props)
		this.state = {
			enableTextInput: false,
			searchText: null,
			showSubmitIcon: false,
			keyboardToggle: false,
		}
	}


	componentDidMount () {
		this.onKeyboardShownRef = Keyboard.addListener('keyboardDidShow', this.onKeyboardShown)
		this.onKeyboardHiddenRef = Keyboard.addListener('keyboardDidHide', this.onKeyboardHidden)
	}

	componentDidUpdate (prevProps, prevState) {
		if (this.state.enableTextInput && this.textInput && prevState.keyboardToggle !== this.state.keyboardToggle)
			this.textInput.focus()
	}

	componentWillUnmount () {
		this.onKeyboardShownRef && this.onKeyboardShownRef.remove()
		this.onKeyboardHiddenRef && this.onKeyboardHiddenRef.remove()
	}

	@bind
	onKeyboardShown () {
		this.setState({ showSubmitIcon: true })
	}

	@bind
	onKeyboardHidden () {
		this.setState({ showSubmitIcon: false })
	}

	@bind
	handleOpenMic () {
		if (this.state.enableTextInput) {
			Keyboard.dismiss()
			this.setState({ enableTextInput: false })
		}
		setTimeout(() => {
			this.props.onAction && this.props.onAction(ACTION.OPEN_MIC)
		}, 200)
	}

	@bind
	openKeyboard () {
		this.setState(prevState => ({ enableTextInput: true, keyboardToggle: !prevState.keyboardToggle }))
	}

	@bind
	handleKeyboard () {
		this.setState({ enableTextInput: true })
	}

	@bind
	handleInputOnChange (event) {
		this.setState({ searchText: event.nativeEvent.text })
	}

	@bind
	isMicOpened () {
		return !this.state.enableTextInput
	}

	@bind
	handleSubmitText () {
		if (!this.state.searchText || this.state.searchText.length === 0)
			return
		const suggestion = { displayText: this.state.searchText }
		const analyticsData = {
			entity_type: 'text_input',
			entity_name: this.state.searchText,
			entity_optional_attributes: { level: 1 },
		}
		this.props.onAction && this.props.onAction(ACTION.POST_SUGGESTION, suggestion, analyticsData)
		this.textInput && this.textInput.clear()
		Keyboard.dismiss()
		this.setState({ searchText: null })
	}

	@bind
	getTextEnabledView () {
		return (
			<View style={ styles.bottomLayout }>
				<View style={ styles.textInputContainer }>
					<TextInput
						ref={ ref => this.textInput = ref }
						keyboardType="default"
						returnKeyType="send"
						style={ styles.textInput }
						underlineColorAndroid="transparent"
						placeholder="Ask something..."
						onChange={ this.handleInputOnChange }
						onSubmitEditing={ this.handleSubmitText }
					/>
				</View>
				{ /* showing either send icon/ voice icon as per config and user input */ }
				{
					this.state.showSubmitIcon || this.props.botILevel === BOT_ILEVEL.TEXT
						? <TouchableOpacity style={ styles.iconContainer } onPress={ this.handleSubmitText }>
							<Image
								style={ styles.iconSend }
								source={ { uri: 'ic_send' } }
								tintColor={ white }
							/>
						</TouchableOpacity>
						: <TouchableOpacity style={ styles.iconContainer } onPress={ this.handleOpenMic }>
							<Image
								style={ styles.icon }
								source={ { uri: 'ic_mic' } }
								tintColor={ white }
							/>
						</TouchableOpacity>
				}
			</View>
		)
	}

	@bind
	getTextDisabledView () {
		return (
			<View style={ styles.bottomLayoutWithoutText }>
				{ !this.props.isSpeechListening ? <TouchableOpacity style={ [ styles.naiveIconContainer, styles.space ] } onPress={ this.handleKeyboard }>
					<Image
						style={ styles.iconKeyboard }
						tintColor={ watermelon100 }
						source={ { uri: 'keyboard' } }
					/>
				</TouchableOpacity>
					: <View style={ styles.space } />
				}
				{
					this.props.isSpeechListening // isSpeechlistening true when voice is recorded so we show the GIF
						? <Image
							style={ [ styles.voiceLoader, styles.space, styles.centerIcon ] }
							source={ { uri: 'speech_loader' } }
						/>
						: <TouchableOpacity style={ [ styles.naiveIconContainer, styles.space, styles.centerIcon ] } onPress={ this.handleOpenMic }>
							<Image
								style={ styles.icon }
								tintColor={ watermelon100 }
								source={ { uri: 'ic_mic' } }
							/>
						</TouchableOpacity>
				}
				<View style={ styles.space } />
			</View>
		)
	}

	render () {
		if (this.props.botILevel === BOT_ILEVEL.SUGGESTION) /* when level is `0` only suggestion input is enabled */
			return (<View />)
		if (this.state.enableTextInput || this.props.botILevel === BOT_ILEVEL.TEXT) /* when level is `1` only suggestion input and text input are enabled */
			return this.getTextEnabledView()
		return this.getTextDisabledView() /* when level is `2` suggestion, text input and speech input are enabled*/
	}
}