import React from 'react'
import PropTypes from 'prop-types'

import {
	View,
	TouchableOpacity,
	Text,
	Image,
	ScrollView,
	ViewPropTypes,
} from 'react-native'
import { ACTION } from '../../constants'
import styles from './styles'

const onPress = (suggestion, currentSuggestion, onAction) => {
	const analyticsData = {
		entity_type: 'query',
		entity_name: currentSuggestion.context && 	(currentSuggestion.context.triggers) || suggestion.trigger,
		entity_optional_attributes: { level: currentSuggestion.context && currentSuggestion.context.level || 1 },
	}
	onAction(ACTION.POST_SUGGESTION, suggestion, analyticsData)
}


const Suggestions = props => {
	const {
		currentSuggestion = {},
		onAction,
		suggestionsContainer,
		suggestionStyle,
	} = props
	if (!currentSuggestion.suggestions || currentSuggestion.suggestions.size < 1)
		return (<View />)


	return (
		<ScrollView
			style={ suggestionsContainer }
			horizontal
			showsHorizontalScrollIndicator={ false }
			keyboardShouldPersistTaps={ 'always' }
		>
			<View style={ suggestionStyle } >
				{
					currentSuggestion.suggestions.map(suggestion => (
						<View style={ styles.pillContainer }>
							<TouchableOpacity
								activeOpacity={ 0.9 }
								onPress={ () => onPress(suggestion, currentSuggestion, onAction) }
							>
								<View style={ styles.flexRow }>
								{ suggestion.imageUrl
								&& <Image
									style={ styles.pillIcon }
									resizeMode={ 'contain' }
									source={ { uri: suggestion.imageUrl } }
								/> }
									<Text style={ suggestion.imageUrl ? styles.pillText : styles.pillTextOnly }>
										{ suggestion.displayText }
									</Text>
								</View>
							</TouchableOpacity>
						</View>
						)
					)
				}
			</View>
		</ScrollView>
	)
}

Suggestions.propTypes = {
	currentSuggestion: PropTypes.object,
	onAction: PropTypes.func,
	suggestionsContainer: ViewPropTypes.style,
	suggestionStyle: ViewPropTypes.style,
}

export default Suggestions