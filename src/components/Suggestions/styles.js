import {
	StyleSheet,
} from 'react-native'
import { cornflowerBlue } from '../../resources/colors'
import fonts from '../../resources/fonts'

const styles = StyleSheet.create({
	container: {
		paddingVertical: 8,
		flexDirection: 'row',
		alignItems: 'flex-end',
		flex: 1,
		justifyContent: 'flex-end',
	},
	pillContainer: {
		borderColor: cornflowerBlue,
		borderWidth: 1,
		borderRadius: 20,
		marginHorizontal: 6,
	},
	pillText: {
		fontFamily: fonts.bold,
		fontSize: 16,
		fontStyle: 'normal',
		letterSpacing: 0,
		paddingVertical: 8,
		paddingRight: 18,
		textAlign: 'center',
		color: cornflowerBlue,
	},
	pillTextOnly: {
		fontFamily: fonts.bold,
		fontSize: 16,
		fontStyle: 'normal',
		letterSpacing: 0,
		paddingVertical: 8,
		paddingHorizontal: 18,
		textAlign: 'center',
		color: cornflowerBlue,
	},
	flexRow: { flexDirection: 'row' },
	pillIcon: {
		width: 20,
		alignSelf: 'center',
		height: 20,
		marginHorizontal: 10,
		marginVertical: 5,
		tintColor: cornflowerBlue,
	},
})

export default styles