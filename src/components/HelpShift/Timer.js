import React from 'react'
import PropTypes from 'prop-types'
import {
	View,
	Text,
} from 'react-native'
import { timeStyles } from './styles'
import { ACTION } from '../../constants'

class Timer extends React.PureComponent {

	constructor (props) {
		super(props)
		this.state = { timerSeconds: 240 }
	}

	componentDidMount () {
		this._secondsTimer = setInterval(() => {
			this.setState(prevState => ({ timerSeconds: prevState.timerSeconds - 1 }))
		}, 1000)
		this._totalTimer = setTimeout(() => {
			this.props.onAction && this.props.onAction(ACTION.HELPSHIFT_TIMEOUT)
			clearInterval(this._secondsTimer)
		}, 1000 * 240)
	}

	componentWillUnmount () {
		clearInterval(this._secondsTimer)
		clearTimeout(this._totalTimer)
	}

	getTimeStr = timerSeconds => {
		const mins = parseInt(timerSeconds / 60)
		const secs = timerSeconds % 60
		return '0' + mins + ':' + (secs > 9 ? secs : '0' + secs)
	}

	render () {
		return (
			<View style={ timeStyles.container }>
				<View style={ timeStyles.timerContainer }>
					<Text style={ timeStyles.firstText }>{ 'Wait time ~'} </Text>
					<Text style={ timeStyles.secondText }>{ this.getTimeStr(this.state.timerSeconds) } </Text>
				</View>
				{ this.props.data.itext && <Text style={ timeStyles.iText }>{ this.props.data.itext }</Text> }
			</View>
		)
	}
}

Timer.propTypes = {
	data: PropTypes.object.isRequired,
	onAction: PropTypes.func,
}

export default Timer