import React from 'react'
import PropTypes from 'prop-types'
import {
	View,
	Animated,
} from 'react-native'
import { bind } from '../../../../utils/decorators'
import { chatWaitingStyles } from './styles'

const ANIMATION_CONFIG = { duration: 400 }

class ChatWaiting extends React.PureComponent {

	constructor (props) {
		super(props)
		this.state = {
			scales: [
				new Animated.Value(1),
				new Animated.Value(1),
				new Animated.Value(1),
				new Animated.Value(1),
			],
		}
	}

	componentDidMount () {
		this.startAndRepeatAnimation()
	}

	componentWillUnmount () {
		this._anime && this._anime.stop()
	}

	@bind
	startAndRepeatAnimation () {
		this.startAnimation(this.startAndRepeatAnimation)
	}

	@bind
	getParallelAnimation (shrinkDot, expandDot) {
		return Animated.parallel([
			Animated.timing(this.state.scales[shrinkDot], {
				...ANIMATION_CONFIG,
				toValue: 1,
			}),
			Animated.timing(this.state.scales[expandDot], {
				...ANIMATION_CONFIG,
				toValue: 1.4,
			}),
		])
	}

	@bind
	startAnimation (repeater) {
		this._anime = Animated.sequence([
			this.getParallelAnimation(3, 0),
			this.getParallelAnimation(0, 1),
			this.getParallelAnimation(1, 2),
			this.getParallelAnimation(2, 3),
		])
		this._anime.start(repeater)
	}

	render () {
		return (
			<View style={ chatWaitingStyles.container }>
				<View style={ chatWaitingStyles.dotsContainer } >
					{ this.state.scales.map(scale =>
						<Animated.View style={ [ chatWaitingStyles.dot, { transform: [ { scale } ] } ] } />
					)}
				</View>
			</View>
		)
	}

}

ChatWaiting.propTypes = { data: PropTypes.object.isRequired }

export default ChatWaiting

