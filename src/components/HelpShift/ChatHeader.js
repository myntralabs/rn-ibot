import React from 'react'
import {
	View,
	Text,
} from 'react-native'
import PropTypes from 'prop-types'
import { chatHeaderStyles } from './styles'

const ChatHeader = ({ data }) => {
	const { itext } = data
	return (
		<View style={ chatHeaderStyles.container }>
			<View style={ chatHeaderStyles.divider } />
			<Text style={ chatHeaderStyles.headerText } >
			{ itext }
			</Text>
		</View>
	)
}

ChatHeader.propTypes = { data: PropTypes.object.isRequired }

export default ChatHeader