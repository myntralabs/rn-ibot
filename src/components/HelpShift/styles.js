import {
	StyleSheet,
	Dimensions,
} from 'react-native'

import fonts from '../../resources/fonts'
import { blueberry10, paleGrey, blueberry40, watermelon100, blueberry60, blueberry50, white, blueberry90 } from '../../resources/colors'

const screenWidth = Dimensions.get('window').width
const timeStyles = StyleSheet.create({
	timerContainer: {
		width: screenWidth,
		height: 25,
		backgroundColor: blueberry10,
		alignItems: 'center',
		justifyContent: 'center',
		flexDirection: 'row',
	},
	container: {
		marginTop: 12,
		width: screenWidth,
		marginBottom: 12,
		flex: 1,
	},
	firstText: {
		fontSize: 12,
		letterSpacing: 0,
		alignSelf: 'center',
		paddingVertical: 6,
		fontFamily: fonts.regular,
		color: blueberry50,
	},
	secondText: {
		fontSize: 12,
		letterSpacing: 0,
		alignSelf: 'center',
		paddingVertical: 6,
		fontFamily: fonts.regular,
		color: watermelon100,
	},
	iText: {
		marginTop: 4,
		letterSpacing: 0,
		textAlign: 'center',
		fontFamily: fonts.regular,
		fontSize: 12,
		color: blueberry90,
		paddingHorizontal: 32,
	},
})

const chatHeaderStyles = StyleSheet.create({
	divider: {
		position: 'absolute',
		top: 10,
		width: screenWidth,
		height: 2,
		backgroundColor: blueberry10,
	},
	headerText: {
		position: 'absolute',
		alignSelf: 'center',
		fontSize: 14,
		color: blueberry60,
		fontFamily: fonts.regular,
		paddingHorizontal: 12,
		backgroundColor: white,
	},
	container: {
		width: screenWidth,
		height: 32,
		marginTop: 16,
		flex: 1,
	},
})

const chatWaitingStyles = StyleSheet.create({
	container: {
		justifyContent: 'flex-start',
		flexDirection: 'row',
		marginVertical: 6,
		marginHorizontal: 8,
	},
	dotsContainer: {
		maxWidth: screenWidth * 0.75,
		flexWrap: 'wrap',
		backgroundColor: paleGrey,
		borderRadius: 18,
		paddingVertical: 8,
		paddingHorizontal: 16,
		flexDirection: 'row',
	},
	dot: {
		width: 5,
		height: 5,
		borderRadius: 5,
		marginVertical: 4,
		marginHorizontal: 3,
		backgroundColor: blueberry40,
	},
})
export { timeStyles, chatHeaderStyles, chatWaitingStyles }