import React from 'react'

import PropTypes from 'prop-types'
import {
	View,
} from 'react-native'
import fonts from '../../../../resources/fonts'
import { darkTwo } from '../../../../resources/colors'
import HTMLView from 'react-native-render-html'
import { richTextStyles } from './styles'

const tagStyles = {
	b: {
		fontFamily: fonts.bold,
		lineHeight: 34,
		letterSpacing: 0,
	},
	p: {
		fontFamily: fonts.regular,
		lineHeight: 34,
		letterSpacing: 0,
	},
}

const fontStyle = {
	color: darkTwo,
	fontFamily: fonts.regular,
	fontSize: 15,
	lineHeight: 26,
}

const RichText = props => {
	const {
		uidx,
		formattedText,
	} = props.data
	const sender = !uidx
	if (!formattedText)
		return (<View />)

	return (
		<View style={ sender ? richTextStyles.containerSender : richTextStyles.containerReceiver } >
			<View style={ [ sender ? richTextStyles.senderTextContainer : richTextStyles.receiverTextContainer, richTextStyles.containerPadding ] }>
				<HTMLView
					html={ formattedText }
					tagStyles={ tagStyles }
					baseFontStyle={ fontStyle }
				/>
			</View>
		</View>
	)
}


RichText.propTypes = { data: PropTypes.object.isRequired }

export default RichText