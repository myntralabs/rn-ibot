import React from 'react'

import PropTypes from 'prop-types'
import {
	View,
} from 'react-native'

import { ATTACHMENT_TYPE } from '../../constants'

import styles from './styles'
import RichText from './RichText'

const itemToCardMap = item => {
	if (item.attachmentType === ATTACHMENT_TYPE.ORDER_TRACKING)
		return RichText
}

const renderItem = (item, onAction) => {
	const Card = itemToCardMap(item)
	return Card
		&& <Card
			data={ item }
			onAction={ onAction }
		/>
}

const StandaLoneCard = ({ data, onAction }) => {
	const { attachments: { items = [] } } = data

	return (
		<View style={ styles.container }>
			{ items.map(item => renderItem(item, onAction))}
		</View>
	)
}


StandaLoneCard.propTypes = {
	data: PropTypes.object.isRequired,
	onAction: PropTypes.func.isRequired,
}

export default StandaLoneCard