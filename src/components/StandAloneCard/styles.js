import {
	StyleSheet,
	Dimensions,
} from 'react-native'

import {
	paleGrey,
	palePurple,
} from '../../resources/colors'

const screenWidth = Dimensions.get('window').width

const richTextStyles = StyleSheet.create({
	containerSender: {
		justifyContent: 'flex-start',
		flexDirection: 'row',
		marginVertical: 6,
		marginHorizontal: 8,
	},
	containerReceiver: {
		justifyContent: 'flex-end',
		flexDirection: 'row',
		marginVertical: 6,
		marginHorizontal: 8,
	},
	senderTextContainer: {
		maxWidth: screenWidth * 0.75,
		flexWrap: 'wrap',
		backgroundColor: paleGrey,
		borderRadius: 18,
	},
	receiverTextContainer: {
		maxWidth: screenWidth * 0.75,
		flexWrap: 'wrap',
		backgroundColor: palePurple,
		borderRadius: 18,
	},
	containerPadding: { paddingHorizontal: 16 },
	font: { fontSize: 16, lineHeight: 26 },
})

const styles = StyleSheet.create({ container: { flex: 1 } })

export { richTextStyles }
export default styles