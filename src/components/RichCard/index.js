import React from 'react'

import PropTypes from 'prop-types'
import {
	View,
	Text,
	FlatList,
	Image,
	Dimensions,
	TouchableOpacity,
} from 'react-native'

import TextCard from '../TextCard'
import { ACTION, RUPEE_UNICODE, ATTACHMENT_TYPE } from '../../constants'
import getCloudinaryUrlFor from '../../../../utils/Cloudinary'

import styles from './styles'
import OrderStatus from '../OrderStatus/index'
import { getAnalyticsData } from '../../utils/helpers'
import { bind } from '../../../../utils/decorators'

const screenWidth = Dimensions.get('window').width
const imageParams = {
	width: Math.floor(screenWidth / 2),
	height: Math.floor((screenWidth / 2) * 1.28),
	quality: 50,
}

class RichCard extends React.PureComponent {
	constructor (props) {
		super(props)
		this.isSender = !(props.data && props.data.uidx)
	}

	componentWillReceiveProps (nextProps) {
		this.isSender = !(nextProps.data && nextProps.data.uidx)
	}

	@bind
	onLocalAction (action, payload = {}, analyticsData) {
		this.props.onAction && this.props.onAction(action, { ...payload, ...this.props.data }, analyticsData)
	}

	@bind
	onActionPress (action, i, currentIndex) {
		const actionPayload = {
			selectedAttachment: currentIndex,
			selectedAction: i, redirectUrl:
			action.redirectUrl,
		}
		const analyticsData = getAnalyticsData('link', this.props.data.context)
		this.onLocalAction(ACTION.ATTACHMENT_ACTION_PERFORMED, actionPayload, analyticsData)
	}

	@bind
	getEachAction (action, i, currentIndex) {
		return (
			<TouchableOpacity
				style={ styles.pillContainer }
				onPress={ () => this.onActionPress(action, i, currentIndex) }
				key={ i }
			>
				<View>
					<Text style={ styles.pillText }>
						{ action.text }
					</Text>
				</View>
			</TouchableOpacity>
		)
	}

	@bind
	getActions (actions, currentIndex) {
		if (!this.props.data)
			return null
		return (
			<View style={ styles.actionsStyle }>
				{
				actions.map((action, i) => this.getEachAction(action, i, currentIndex))
				}
			</View>
		)
	}

	@bind
	onOrderStatusPressed (redirectUrl) {
		const analyticsData = getAnalyticsData('link', this.props.data.context)
		this.props.onAction(ACTION.ATTACHEMENT_REDIRECT, { redirectUrl }, analyticsData)
	}

	@bind
	getOrderStatus (status, redirectUrl) {
		return (<View style={ styles.actionContainer } >
			<OrderStatus
				tracking={ status.states }
				onAction={ () => this.onOrderStatusPressed(redirectUrl) }
			/>
		</View>)
	}

	@bind
	getCardContent (item, currentIndex, imageUrl) {
		if (!item || !item.formattedText)
			return (<View />)
		const otherData = typeof item.formattedText === 'string' ? JSON.parse(item.formattedText) : item.formattedText

		if (this.isSender)
			return (
				<View style={ styles.flex }>
					<View style={ [ styles.flexRow, styles.itemContainer ] }>
						<Image
							style={ styles.itemImage }
							source={ { uri: imageUrl } }
						/>
						<View style={ styles.itemContentContainer }>
							<Text style={ styles.brandText } numberOfLines={ 1 }>{ otherData.brand }</Text>
							<Text
								style={ styles.nameText }
								numberOfLines={ 1 }
							>{ otherData.name }</Text>
							<Text style={ styles.nameText }>{ `Size: ${ otherData.size } | Qty: ${ otherData.quantity }` }</Text>
							<Text style={ styles.priceText }>{ RUPEE_UNICODE + otherData.price }</Text>
							<View style={ styles.flexRow }>
			{ otherData.status && otherData.status.currentStatus && <Text style={ otherData.status.currentStatus === 'Delivered' ? styles.deliveryTextSuccess : styles.deliveryTextNormal }>{ otherData.status.currentStatus }</Text> }
								<Text style={ styles.deliveryDateText }> { `(${ otherData.createdon })` }</Text>
							</View>
						</View>
					</View>
					<View style={ styles.divider } />
					{ item.actions
						? <View style={ styles.actionContainer }>
							{ this.getActions(item.actions, currentIndex) }
						</View>
						: otherData.status && otherData.status.states && otherData.status.states.length > 0
						&& <View style={ styles.statusContainer } >
							{ this.getOrderStatus(otherData.status, otherData.redirectUrl) }
						</View>
					}
				</View>
			)
		return (
			<View>
				<Image
					style={ styles.senderProductImage }
					source={ { uri: imageUrl } }
				/>
			</View>
		)
	}

	@bind
	getCard (item, index, imageUrl) {
		const analyticsData = getAnalyticsData('link', this.props.data.context)
		return (
			<TouchableOpacity
				style={ styles.flex }
				onPress={ () => item.attachmentType === ATTACHMENT_TYPE.ORDER_SELECT && this.isSender ? this.onLocalAction(ACTION.ATTACHMENT_SELECT_PERFORMED, { selectedAttachment: index }, analyticsData) : this.props.onAction(ACTION.ATTACHEMENT_REDIRECT, { redirectUrl: item.redirectUrl }, analyticsData) }
			>
				{ this.getCardContent(item, index, imageUrl)}
			</TouchableOpacity>
		)
	}

	@bind
	renderItem (data) {
		const {
			item,
			index,
		} = data

		const imageUrl = getCloudinaryUrlFor(item.imageUrl, imageParams)

		return (
			<View style={ styles.flexContainer } >
				<View style={ this.isSender ? styles.mainItemContainer : styles.senderItemContainer }>
					{ this.getCard(item, index, imageUrl) }
				</View>
			</View>
		)
	}

	render () {
		const { attachments: { items = [] } } = this.props.data
		return (
			<View style={ this.isSender ? styles.containerSender : styles.containerReceiver }>
				<View>
					<TextCard
						data={ this.props.data }
						onAction={ this.props.onAction }
					/>
					<View style={ styles.mainContainer } >
						<FlatList
							horizontal
							showsHorizontalScrollIndicator={ false }
							renderItem={ this.renderItem }
							data={ items }
						/>
					</View>
				</View>
			</View>
		)
	}
}

RichCard.propTypes = {
	data: PropTypes.object.isRequired,
	onAction: PropTypes.func.isRequired,
}

export default RichCard