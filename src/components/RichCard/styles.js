import {
	StyleSheet,
	Dimensions,
} from 'react-native'

import {
	white,
	blueberry10,
	blueberry90,
	blueberry60,
	blueberry80,
	cornflowerBlue,
	topaz,
	almostWhite,
} from '../../resources/colors'
import fonts from '../../resources/fonts'

const screenWidth = Dimensions.get('window').width

const styles = StyleSheet.create({
	flex: { flex: 1 },
	margin: { marginVertical: 8 },
	containerSender: {
		justifyContent: 'flex-start',
		flexDirection: 'row',
		marginVertical: 12,
		marginHorizontal: 4,
	},
	containerReceiver: {
		justifyContent: 'flex-end',
		flexDirection: 'row',
		margin: 12,
	},
	mainContainer: {
		backgroundColor: white,
		marginLeft: 4,
	},
	flexContainer: { flex: 1 },
	mainItemContainer: {
		height: 192,
		width: 324,
		borderRadius: 4,
		backgroundColor: almostWhite,
		elevation: 2,
		marginVertical: 8,
		marginHorizontal: 8,
	},
	itemContainer: { padding: 12 },
	senderItemContainer: {
		height: 129,
		width: 104,
		borderRadius: 4,
		elevation: 2,
		marginVertical: 6,
		marginHorizontal: 8,
	},
	itemImage: {
		width: 84,
		height: 110,
		borderColor: blueberry10,
		borderWidth: 1,
	},
	pillContainer: {
		borderColor: cornflowerBlue,
		borderWidth: 1,
		borderRadius: 8,
		marginHorizontal: 6,
	},
	actionsStyle: {
		paddingVertical: 4,
		flexDirection: 'row',
		alignItems: 'flex-start',
		flex: 1,
		marginLeft: 3,
		justifyContent: 'flex-start',
		backgroundColor: white,
	},
	actionContainer: {
		flex: 1,
		marginTop: 6,
	},
	statusContainer: { flex: 1 },
	suggestionsContainer: {
		paddingRight: 8,
		width: screenWidth,
	},
	pillText: {
		fontFamily: fonts.bold,
		fontSize: 16,
		fontStyle: 'normal',
		letterSpacing: 0,
		paddingVertical: 7,
		paddingHorizontal: 11,
		textAlign: 'center',
		color: cornflowerBlue,
	},
	flexRow: { flexDirection: 'row' },
	senderProductImage: {
		height: 129,
		width: 104,
		borderRadius: 4,
	},
	brandText: {
		fontSize: 14,
		fontFamily: fonts.bold,
		color: blueberry90,
		letterSpacing: 0,
		lineHeight: 22,
	},
	nameText: {
		fontSize: 14,
		fontFamily: fonts.regular,
		color: blueberry60,
		flex: 0.8,
		letterSpacing: 0,
		lineHeight: 22,
	},
	priceText: {
		fontSize: 14,
		fontFamily: fonts.bold,
		color: blueberry80,
		letterSpacing: 0,
		lineHeight: 22,
	},
	deliveryTextNormal: {
		fontSize: 14,
		lineHeight: 22,
		letterSpacing: 0,
		fontFamily: fonts.bold,
		color: blueberry90,
	},
	deliveryTextSuccess: {
		fontSize: 14,
		lineHeight: 22,
		letterSpacing: 0,
		fontFamily: fonts.bold,
		color: topaz,
	},
	deliveryDateText: {
		fontSize: 14,
		lineHeight: 22,
		letterSpacing: 0,
		fontFamily: fonts.regular,
		color: blueberry90,
	},
	itemContentContainer: {
		paddingLeft: 12,
		paddingTop: 4,
		flex: 1,
	},
	divider: {
		height: 1,
		width: 350,
		backgroundColor: blueberry10,
	},
})

export default styles