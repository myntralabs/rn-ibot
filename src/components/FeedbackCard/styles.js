import {
	StyleSheet,
	Dimensions,
} from 'react-native'

import fonts from '../../resources/fonts'
import { blueberry10, white, blueberry90 } from '../../resources/colors'

const screenWidth = Dimensions.get('window').width
const styles = StyleSheet.create({
	container: {
		flex: 1,
		width: screenWidth,
		height: 132,
	},
	dividerLike: {
		position: 'absolute',
		top: 28,
		width: screenWidth,
		height: 2,
		backgroundColor: blueberry10,
	},
	dividerDislike: {
		position: 'absolute',
		top: 26,
		width: screenWidth,
		height: 2,
		backgroundColor: blueberry10,
	},
	reaction: {
		position: 'absolute',
		alignSelf: 'center',
		fontSize: 26,
		fontFamily: fonts.bold,
		paddingHorizontal: 12,
		top: 12,
		backgroundColor: white,
	},
	contentText: {
		alignSelf: 'center',
		textAlign: 'center',
		fontFamily: fonts.bold,
		fontSize: 18,
		paddingHorizontal: 20,
		color: blueberry90,
		marginTop: 52,
	},
})

export default styles