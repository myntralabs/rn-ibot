import React from 'react'
import PropTypes from 'prop-types'
import {
	View,
	Text,
} from 'react-native'
import styles from './styles'

import {
	FEEDBACK_POSITIVE_MESSAGE,
	FEEDBACK_NEGATIVE_MESSAGE,
	THUMBS_UP,
	THUMBS_DOWN,
} from '../../constants'

const FeedbackCard = ({ data }) => {
	const { trigger } = data
	const message = trigger === 'great' ? FEEDBACK_POSITIVE_MESSAGE : FEEDBACK_NEGATIVE_MESSAGE
	const icon = trigger === 'great' ? THUMBS_UP : THUMBS_DOWN
	return (
		<View style={ styles.container }>
			<View style={ trigger === 'great' ? styles.dividerLike : styles.dividerDislike } />
			<Text style={ styles.reaction }>{ icon }</Text>
			<Text style={ styles.contentText }>{ message }</Text>
		</View>
	)
}

FeedbackCard.propTypes = { data: PropTypes.object.isRequired }

export default FeedbackCard