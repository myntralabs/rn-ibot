import React from 'react'

import Chat from './components/Chat'
import PropTypes from 'prop-types'
import MqttHelper from './mqttClient'

import {
	NativeModules,
	Linking,
	DeviceEventEmitter,
} from 'react-native'

import sendAnalyticsEvent from './analytics'
import {
	ACTION,
	COMMON_APP_LINK_PATTERNS,
	TRIGGER,
	INIT_TRIGGER,
	END_CHAT_TRIGGER,
	BOT_ILEVEL,
	ListenerEvents,
} from './constants'
import { mergeMessages, isAcknowledgementChat, removeAckMessages, guid } from './utils/helpers'
import bind from './utils/decorators'

const SpeechRecognizerModule = NativeModules.SpeechRecognizerModule

class ChatAssistant extends React.PureComponent {
	constructor (props) {
		super(props)
		this.state = {
			imessages: [],
			currentSuggestions: {},
			isSpeechListening: false,
			botILevel: BOT_ILEVEL.SUGGESTION,
		}
		this.retryCount = 0
		this.pageRef = component => this.page = component
		this.chatRef = chat => this.chat = chat
		this.isInit = true
		this.speechPermissionGiven = false
		this.initProps(props)
	}


	static getDerivedStateFromProps (nextProps, prevState) {
		this.initProps(nextProps)
		if (nextProps.config && prevState.botILevel !== nextProps.config.botILevel)
			return { botILevel: nextProps.config.botILevel }
		return null
	}

	componentDidMount () {
		this.init()
	}

	componentWillUnmount () {
		if (this.mqttClient) {
			this.mqttClient.disconnect()
			MqttHelper.removeClient(this.mqttClient)
		}
		this.eventHandler && this.eventHandler.remove()
		this.navListeners
			&& this.navListeners.forEach(navListener => {
				navListener.remove()
			})
		SpeechRecognizerModule.onUnMount()
	}

	@bind
	handleOnAppear () {
		this.pageLive = true
		this.mqttClient && this.mqttClient.connect()
		this.eventHandler = DeviceEventEmitter.addListener('speechStatus', this.handleSpeech)
	}

	@bind
	handleOnDisappear () {
		this.pageLive = false
		this.mqttClient && this.mqttClient.disconnect()
		this.eventHandler && this.eventHandler.remove()
		SpeechRecognizerModule.cancelDetection()
	}

	@bind
	initProps (props) {
		if (props.config) {
			this.baseUrl = props.config.baseUrl
			this.baseUrlMqtt = props.config.baseUrlMqtt
			this.groupType = props.config.groupType
			this.groupName = props.config.groupName
		}
	}

	@bind
	getScreenObject () {
		return {
			name: 'Customer Care bot',
			url: this.props.url,
			referrer: this.props.referrer,
			data_set: {},
		}
	}


	@bind
	init () {
		this.connectAndSubscribeToMqtt()
	}


	@BiquadFilterNode
	openMic (speechPermissionGiven = false) {
		if (!speechPermissionGiven)
			this.props.handlePermission && this.props.handlePermission()
		else
			SpeechRecognizerModule.startDetection()
	}

	@bind
	handleSpeech (eventData) {
		if (eventData.hasOwnProperty(ListenerEvents.SpeechStarted))
			// Detection has started. Start the animation
			this.setState({ isSpeechListening: true })
		else if (eventData.hasOwnProperty(ListenerEvents.SpeechFound)) {
			// Found the Speech
			const returnedText = eventData[ListenerEvents.SpeechFound]
			if (returnedText) {
				const suggestion = { displayText: returnedText }
				const analyticsData = {
					entity_type: 'speech_input',
					entity_name: this.state.searchText,
					entity_optional_attributes: { level: 1 },
				}
				this.onAction(ACTION.POST_SUGGESTION, suggestion, analyticsData)
			}
			this.setState({ isSpeechListening: false })
		} else if (eventData.hasOwnProperty(ListenerEvents.SpeechEnded))
			// Speech has ended. Stop the animation
			this.setState({ isSpeechListening: false })
		else if (eventData.hasOwnProperty(ListenerEvents.SpeechError))
			// Found the error
			this.setState({ isSpeechListening: false })
	}

	@bind
	connectAndSubscribeToMqtt () {
		MqttHelper.createClient({
			uri: this.baseUrlMqtt,
			clientId: guid(),
		}).then(client => {
			this.mqttClient = client
			client.on('closed', this.onConnectionClosed)
			client.on('message', this.onAsyncMessageReceived)
			client.on('connect', this.onConnected)
			client.on('error', this.onConnectionError)
			client.on('subscribe', this.onSubscribed)
			client.connect()
		}).catch(err => {
			this.onErrorConnectingToMqtt(err)
		})
	}

	@bind
	onConnected () {
		const topic = `/v1/ibot/${ this.groupName }${ this.uidx }`
		this.mqttClient && this.mqttClient.subscribe(topic, 2)
		this.retryCount = 0
	}

	@bind
	onSubscribed () {
		if (this.isInit) {
			this.postSuggestion(INIT_TRIGGER, false)
			this.isInit = false
		}
	}

	@bind
	onAsyncMessageReceived (msg) {
		if (msg.data && msg.data) {
			const messageStr = JSON.parse(msg.data)
			if (messageStr.response) {
				const message = messageStr.response
				message.forEach(msg => this.handleMessageActions(msg))
				if (message.some(msg => isAcknowledgementChat(msg, this.state.imessages)))
					return
				this.updateMessagesState(message)
			}
		}
	}

	@bind
	onSyncMessageReceived (messages) {
		messages.forEach(msg => this.handleMessageActions(msg))
		if (messages.some(msg => isAcknowledgementChat(msg, this.state.imessages)))
			return
		this.updateMessagesState(messages)
	}

	@bind
	handleMessageActions (message) {
		if (!message)
			return

		if (message.trigger === TRIGGER.CHAT_ASSIGNED
			|| message.trigger === TRIGGER.CHAT_ENDED)
			this.onAction(ACTION.CHAT_INIT)

		if (message.trigger === TRIGGER.CHAT_ENDED)
			this.onAction(ACTION.CHAT_ENDED)

		if (message.trigger === TRIGGER.LOGIN)
			this.onAction(ACTION.LOGIN)
	}

	@bind
	onConnectionClosed () {
		if (this.retryCount < 3) {
			this.mqttClient && this.mqttClient.connect()
			this.retryCount += 1
		}
	}

	@bind
	onConnectionError (msg) {
		if (!this.pageLive || msg && typeof msg === 'string' && msg.search('321') >= 0) // known mqtt errors start with 321 so we ignore it
			return
		this.mqttClient = null
	}

	@bind
	onAction (type, data = {}, analyticsData = {}) {
		const screen = this.getScreenObject()
		screen.data_set = analyticsData
		switch (type) {
			case ACTION.POST_SUGGESTION: {
				sendAnalyticsEvent({ screen })
				this.postSuggestion(data)
				this.redirectIfNeeded(data)
				break
			}
			case ACTION.ATTACHMENT_ACTION_PERFORMED: {
				sendAnalyticsEvent({ screen })
				this.postAction(data)
				this.redirectIfNeeded(data)
				break
			}
			case ACTION.CLEAR_ACK_MESSAGES: {
				this.setState(prevState => ({ imessages: prevState.imessages.filter(message => message.trigger !== TRIGGER.CHAT_ACK) }))
				break
			}
			case ACTION.ATTACHEMENT_REDIRECT: {
				this.redirectIfNeeded(data)
				break
			}
			case ACTION.ATTACHMENT_SELECT_PERFORMED: {
				sendAnalyticsEvent({ screen })
				this.postSelection(data)
				break
			}
			case ACTION.HELPSHIFT_TIMEOUT : {
				this.helpShiftTimedOut()
				break
			}
			case ACTION.CHAT_INIT : {
				/* removing 'Wait timer' CARD once chat is initiated*/
				this.setState(prevState => ({ imessages: prevState.imessages.filter(message => !(message.trigger === 'chat-with-us' && !message.fromUser)) }))
				break
			}
			case ACTION.CHAT_ENDED: {
				this.currentTrigger = null
				if (this.currentbotLevel === BOT_ILEVEL.SUGGESTION)
					this.setState({ botILevel: BOT_ILEVEL.SUGGESTION })
				break
			}
			case ACTION.OPEN_MIC: {
				this.openMic()
				break
			}
			case ACTION.LOGIN: {
				this.props.handleLogin && this.props.handleLogin()
				break
			}
		}
	}

	@bind
	updateMessagesState (newMessages) {
		let imessages = mergeMessages(this.state.imessages, newMessages)
		newMessages.forEach(newMessage => {
			imessages = removeAckMessages(imessages, newMessage)
		})
		const currentSuggestions = imessages && imessages.length > 0 ? imessages[imessages.length - 1] : {}
		this.setState({
			imessages,
			currentSuggestions,
		})
	}

	@bind
	redirectIfNeeded (data) {
		const url = data.redirectUrl
		if (url && url.search('/cc/helpshift') >= 0 || data.trigger === TRIGGER.CHAT_INIT) {
			this.currentTrigger = data.trigger
			this.chat && this.chat.openKeyboard()
			this.currentbotLevel = this.state.botILevel
			if (this.currentbotLevel === BOT_ILEVEL.SUGGESTION)
				this.setState({ botILevel: BOT_ILEVEL.TEXT })
		}
		if (url && COMMON_APP_LINK_PATTERNS.some(reg => reg.test(url)))
			Linking.openURL(url)
		else
			this.props.handleNavigation && this.props.handleNavigation(url)
	}


	@bind
	postSuggestion (suggestion = {}, shouldUpdate = true) {
		this.lastSuggestion = suggestion
		const redirectUrl = suggestion.redirectUrl
		if (!suggestion.displayText || redirectUrl && redirectUrl.search('/cc/helpshift') >= 0 || suggestion.trigger === TRIGGER.CHAT_INIT)
			return
		const data = {
			groupType: this.groupType,
			groupName: this.groupName,
			data: [ {
				itext: suggestion.displayText,
				trigger: suggestion.trigger || this.currentTrigger,
			} ],
		}
		const url = `${ this.baseUrl }/messages/v1/`
		const newMessage = {
			groupType: this.groupType,
			groupName: this.groupName,
			fromUser: true,
			itext: suggestion.displayText,
			suggestions: [],
		}
		if (shouldUpdate) {
			this.updateMessagesState([ newMessage ])
			this.onAction(ACTION.CLEAR_ACK_MESSAGES)
		}

		this.props.post && this.props.post(url, data).then(result => {
			const { body: { response = [] } } = result
			this.onSyncMessageReceived(response)
		})
	}


	@bind
	postAction (data) {
		const postData = Object.assign({}, data)
		if (postData.redirectUrl)
			return
		if (!(postData
				&& postData.attachments
				&& postData.attachments.items
				&& postData.selectedAttachment >= 0
				&& postData.selectedAction >= 0))
			return
		postData.attachments.items = postData.attachments.items.map((attachment, index) => {
			if (index === postData.selectedAttachment)
				attachment.actions = attachment.actions.map((action, i) => {
					action.performed = i === postData.selectedAction
					return action
				}).filter(action => action.performed)
			return attachment
		}).filter((attachment, index) => index === postData.selectedAttachment)
		const url = `${ this.baseUrl }/messages/v1/`
		delete postData.selectedAttachment
		delete postData.selectedAction
		const payload = {
			groupType: this.groupType,
			groupName: this.groupName,
			data: [ postData ],
		}

		this.props.post && this.props.post(url, payload).then(result => {
			const { body: { response = [] } } = result
			this.onSyncMessageReceived(response)
		})
	}

	@bind
	postSelection (data) {
		const postData = Object.assign({}, data)
		if (!(postData
			&& postData.attachments
			&& postData.attachments.items
			&& postData.selectedAttachment >= 0))
			return
		postData.attachments.items = postData.attachments.items.map((attachment, index) => {
			attachment.selected = index === postData.selectedAttachment
			return attachment
		})
		const url = `${ this.baseUrl }/messages/v1/`
		const newMessage = {
			groupType: this.groupType,
			groupName: this.groupName,
			attachments: { ...postData.attachments, items: [ postData.attachments.items[postData.selectedAttachment] ] },
			suggestions: [],
			fromUser: true,
		}
		delete postData.selectedAttachment
		const payload = {
			groupType: this.groupType,
			groupName: this.groupName,
			data: [ postData ],
		}

		this.props.post && this.props.post(url, payload).then(result => {
			const { body: { response = [] } } = result
			this.onSyncMessageReceived(response)
		})
		this.updateMessagesState([ newMessage ])
	}

	@bind
	helpShiftTimedOut () {
		this.postSuggestion(END_CHAT_TRIGGER, false)
		this.setState(prevState => ({ imessages: prevState.imessages.filter(message => !(message.trigger === 'chat-with-us' && !message.fromUser)) }))
	}

	@bind
	render () {
		return (
			<Chat
				imessages={ this.state.imessages || [] }
				onAction={ this.onAction }
				currentSuggestions={ this.state.currentSuggestions || {} }
				isSpeechListening={ this.state.isSpeechListening }
				botILevel={ this.state.botILevel }
				ref={ this.chatRef }
			/>
		)
	}

}


ChatAssistant.propTypes = {
	config: PropTypes.PropTypes.shape({
		baseUrlMqtt: PropTypes.string.isRequired,
		baseUrl: PropTypes.string.isRequired,
		botILevel: PropTypes.PropTypes.oneOf([ 0, 1, 2 ]).isRequired,
		groupType: PropTypes.string.isRequired,
		groupName: PropTypes.string.isRequired,
	}).isRequired,
	handleLogin: PropTypes.func.isRequired,
	handleNavigation: PropTypes.func.isRequired,
	handlePermission: PropTypes.func.isRequired,
	post: PropTypes.func.isRequired,
	url: PropTypes.string,
	referrer: PropTypes.any,
}

export default ChatAssistant
/* required configs from client app
	baseUrlMqtt,
	baseUrl,
	botILevel,
	groupType,
	groupName
*/