import { Platform } from 'react-native'

export default Platform.select({
	android: {
		regular: 'Whitney-Book-Bas',
		bold: 'Whitney-Semibold-Bas',
		italics: 'Whitney-BookItal-Bas',
		medium: 'Whitney-Medium',
	},
	ios: {
		regular: 'Whitney-Book',
		bold: 'Whitney-Semibold',
		italics: 'Whitney-BookItalic',
		medium: 'Whitney-Medium',
	},
})
