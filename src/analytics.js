import sendEvent from '../../utils/MynacoEvent'


const sendAnalyticsEvent = analyticsData => {
	const eventPayload = {
		...analyticsData,
		event: 'widgetItemClick',
		eventType: analyticsData.eventType || 'entity_event',
	}
	sendEvent(eventPayload)
}

export default sendAnalyticsEvent
