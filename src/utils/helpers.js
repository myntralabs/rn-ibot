import { TRIGGER } from '../constants'

export function mergeMessages (oldMessages, newMessages) {
	return [ ...oldMessages, ...newMessages ]
}

export function removeAckMessages (allMessages, newMessage) {
	if (newMessage && newMessage.trigger !== TRIGGER.CHAT_ACK)
		return allMessages.filter(message => message.trigger !== TRIGGER.CHAT_ACK)
	return allMessages
}

export function isAcknowledgementChat (message, imessages) {
	return message
		&& message.trigger === TRIGGER.CHAT_ACK
		&& imessages
		&& imessages[imessages.length - 1]
		&& imessages[imessages.length - 1].trigger === TRIGGER.CHAT_ACK
}

export function getTriggers (triggers) {
	if (!triggers)
		return
	return triggers.reduce((acc, trigger) => acc + ',' + trigger)
}

export function getAnalyticsData (entityType, context) {
	return {
		entity_type: entityType,
		entity_name: context && getTriggers(context.triggers),
		entity_optional_attributes: { level: context && context.level || 1 },
	}
}

/*
to provide random guid as MQTT client id in case the uidx is not there(non logged in flow)
*/
export function guid () {
	let currentDateMilliseconds = new Date().getTime()
	return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, currentChar => {
		const randomChar = (currentDateMilliseconds + Math.random() * 16) % 16 | 0
		currentDateMilliseconds = Math.floor(currentDateMilliseconds / 16)
		return (currentChar === 'x' ? randomChar : (randomChar & 0x7 | 0x8)).toString(16)
	})
}
