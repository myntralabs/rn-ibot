export default function bind (target, key, descriptor) {
	let fn = descriptor.value
	return {
		configurable: true,
		get () {
			return fn.bind(this)
		},
		set (value) {
			fn = value
		},
	}
}
